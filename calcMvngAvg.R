rm(list=ls())
##
# Übungen zur Zeitreihenanalyse
# 
# Berechnung des "Mooving Averages" auf eine Zeitreihe, 
# anschließend Erstellung eines Plots mit den Rohdaten, 
# als auch mit den errechneten mooving averages.
#
# @author Benedict Ernst
# @date 23. April 2013 
##

# Passagier Daten initialisieren
data <- AirPassengers

## 
# calcMvngAvg
#
# Die Funktion errechnet den "Mooving Average" mit einer frei bestimmbaren Frequenz.
# 
# @param    Time-Series Objects   data            // Die zu verarbeitende Zeitreihe
# @param    Integer               frequency       // Die zu analysierende Frequency
#
# @return   Time-Series Objects
##
calcMvngAvg <- function( data, freq = NULL ) {
  # Ausgabe initialisieren
  result <- NULL
  
  # Frequency
  if(is.null(freq)) { # Wenn keine Frequency manuell eingetragen wurde, wird die des Datensatzes benutzt
    freq <- frequency(data)
  }
  halfFreq <- freq/2  # halbe Frequenz berechnen 
  
  # Felder durchlaufen
  for (i in 1:length(data)) {
    if( i < halfFreq | i > length(data)-halfFreq ) { 
      # Ergebnis speichern
      result <- c(result, data[i]) 
    } else { 
      # Start und Ende des MvngAvg berechnen
      start <- i - halfFreq
      startMiddle <- i - halfFreq + 1
      end <- i + halfFreq
      endMiddle <- i + halfFreq - 1
      # Durchschnitt berechnen
      mvngAvg <- mean( c((data[start]/2), data[startMiddle:endMiddle], (data[end]/2)) )
      # Ergebnis speichern
      result <- c(result, mvngAvg)
    }
  }
  
  # Ergebnis wieder als Time-Series Object umwandeln
  result.ts <- ts(result, start=start(data), end=end(data), frequency=frequency(data))
  
  # Ausgabe
  return(result.ts)
}; dataMvngAvg <- calcMvngAvg(data)

# Plots erstellen
# Plots untereinander
layout(1:2)
plot(data, type='s', main='Zeitreihe mit Rohdaten', xlab='Jahr', ylab='Passagiere in Tsd.')
plot(dataMvngAvg, type='s', main='Bereinigte Zeitreihe', xlab='Jahr', ylab='Passagiere in Tsd.')
# Plots aufeinander
layout(1)
plot(data, type='s', main='Zeitreihe mit überlagerter Glättung', xlab='Jahr', ylab='Passagiere in Tsd.')
lines(dataMvngAvg, col="red")

# Daten um Trend bereinigen
plot(data - dataMvngAvg, type='s', main='Zeitreihe mit bereinigten Daten', xlab='Jahr', ylab='Passagiere in Tsd.')