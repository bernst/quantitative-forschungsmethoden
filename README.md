Quantitative Forschungsmethoden
========================================================

# Topic
Thema der Veranstaltung ist die Analyse und Prognose der Dynamik ökonomischer Variablen im Zeitverlauf. Reale Daten wie Passagierzahlen von Flugunternehmen, Aktienkurs- oder Zinsentwicklungen werden am Rechner mit Standardmodellen der Zeitreihenanalyse (ARIMA-, GARCH- und VAR-Modelle) untersucht.

# Goal 
Fähigkeit, fortgeschrittene quantitative Methoden zur Zeitreihenanalyse unter Einsatz von IT-Instrumenten anzuwenden.

# Contribute
Benedict Ernst @ernstzunehmend